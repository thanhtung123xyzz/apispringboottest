package com.test.init;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.test.model.Post;
import com.test.repository.PostRepository;

@Component
public class Initializer implements CommandLineRunner {

	private final PostRepository postRepository;

	public Initializer(PostRepository postRepository) {
		this.postRepository = postRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		Stream.of("Post 1", "Post 2", "Post 3", "Post 4", "Post 5" )
		.forEach(content -> postRepository.save(new Post(content)));
		postRepository.findAll().forEach(System.out::println);
		
	}

}
