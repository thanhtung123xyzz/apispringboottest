package com.test.service;

import java.util.List;

import com.test.model.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
	List<Post> listPost();
	
	ResponseEntity<?> finById(Long postId);
	
	Post savePost(Post post);
	
	void deletePost(Long postId); 
	
}
