package com.test.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.model.Post;
import com.test.service.PostService;

@RestController
@RequestMapping("/api")
public class PostController {

	private final Logger log = LoggerFactory.getLogger(PostController.class);

	@Autowired
	PostService postService;

	@GetMapping("/post/list")
	List<Post> list() {
		return postService.listPost();
	}

	@GetMapping("/post/{id}")
	ResponseEntity<?> getPost(@PathVariable Long postId) {
		return postService.finById(postId);
	}

	@PostMapping("/post")
	ResponseEntity<Post> createPost(@Valid @RequestBody Post post) throws URISyntaxException {
		Post newPost = postService.savePost(post);
		return ResponseEntity.created(new URI("/api/post" + newPost.getId())).body(newPost);
	}

	@PutMapping("/post")
	ResponseEntity<Post> updatePost(@Valid @RequestBody Post post) {
		Post newPost = postService.savePost(post);
		return ResponseEntity.ok().body(newPost);
	}

	@DeleteMapping("/post/{id}")
	ResponseEntity deletePost(@PathVariable Long postId) {
		postService.deletePost(postId);
		return ResponseEntity.ok().build();
	}

}
