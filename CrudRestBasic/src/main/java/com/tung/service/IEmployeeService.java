package com.tung.service;

import java.util.List;

import com.tung.model.Employee;

public interface IEmployeeService {
	//ham them nhan vien
	Employee addEmployee(Employee employee);
	
	//chinh sua thong tin nhan vien
	Employee updateEmployee(long id, Employee employee);
	
	//xoa nhan vien
	boolean deleteEmployee(long id);
	
	//lay ra danh sach nhan vien
	List<Employee> getAllEmployees();
	
	//lay ra 1 nhan vien
	Employee getOneEmployee(long id);
	
	

}
