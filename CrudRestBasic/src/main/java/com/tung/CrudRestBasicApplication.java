package com.tung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudRestBasicApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudRestBasicApplication.class, args);
	}

}
