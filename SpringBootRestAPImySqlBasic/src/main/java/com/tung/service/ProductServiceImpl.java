package com.tung.service;

import java.util.List;

import javax.print.attribute.standard.MediaSize.Other;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tung.entity.Product;
import com.tung.repository.ProductRepository;

@Service
public class ProductServiceImpl implements IProductService {
	
	@Autowired
	private ProductRepository productRepository;

	@Override
	public Product saveProduct(Product product) {
		return productRepository.save(product);
	}
	
	@Override
	public List<Product> saveAll(List<Product> products) {
		return productRepository.saveAll(products);
	}
	
	@Override
	public List<Product> getAll() {
		return productRepository.findAll();
	}
	
	@Override
	public Product getProductById(Integer id) {
		return productRepository.findById(id).orElse(null);
	}
	
	@Override
	public Product getProductByName(String name) {
		return productRepository.findByName(name);
	}

	@Override
	public Product updateProduct(Product product) {
		Product update = productRepository.findById(product.getId()).orElse(null);
		update.setName(product.getName());
		update.setQuantity(product.getQuantity());
		update.setPrice(product.getPrice());
		
		return productRepository.save(update);
	}

	@Override
	public String deleteProduct(Integer id) {
		productRepository.deleteById(id);
		
		return "Product remove !! " +id;
	}

	
	
}
