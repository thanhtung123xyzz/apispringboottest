package com.tung.service;
import java.util.List;

import com.tung.entity.Product;

public interface IProductService {
	
	Product saveProduct(Product product);
	
	List<Product> saveAll(List<Product> products);
	
	Product updateProduct(Product product);
	
	String deleteProduct(Integer id);
	
	List<Product> getAll();
	
	Product getProductById(Integer id);
	
	Product getProductByName(String name);
}
