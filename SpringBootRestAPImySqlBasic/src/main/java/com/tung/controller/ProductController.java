package com.tung.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tung.entity.Product;
import com.tung.service.IProductService;

@RestController
@RequestMapping("/prd")
public class ProductController {
	
	@Autowired
	IProductService iProductService;
	
	@PostMapping("/addProduct")
	public Product addProduct(@RequestBody Product product) {
		return iProductService.saveProduct(product);
	}
	
	@PostMapping("/addProducts")
	public List<Product> addAllProduct(@RequestBody List<Product> products){
		return iProductService.saveAll(products);
	}
	
	@GetMapping("/products")
	public List<Product> findAllProducts(){
		return iProductService.getAll();
	}
	
	@GetMapping("/productID/{id}")
	public Product	findProductById(@PathVariable Integer id) {
		return iProductService.getProductById(id);
	}
	
	@GetMapping("/product/{name}")
	public Product	findProductByName(@PathVariable String name) {
		return iProductService.getProductByName(name);
	}
	
	@PutMapping("/update")
	public Product updateProduct(@RequestBody Product proudct) {
		return iProductService.updateProduct(proudct);
	}
	
	@DeleteMapping("/delete/{id}")
	public String deleteProduct(@PathVariable Integer id) {
		return iProductService.deleteProduct(id);
	}
}
