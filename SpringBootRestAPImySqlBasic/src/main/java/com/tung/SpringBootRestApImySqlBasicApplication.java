package com.tung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestApImySqlBasicApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestApImySqlBasicApplication.class, args);
	}

}
